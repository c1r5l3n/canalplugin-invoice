<?php


namespace Invoice;


use Exception;
use Flood\Canal\Url\Url;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

class Mail {


    public function sendMail() {
        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
        if($contentType === "application/json") {
            $email_and_filename = file_get_contents('php://input');
            $email_and_filename_decoded = json_decode($email_and_filename, true);
            $receiver = $email_and_filename_decoded['email'];
            $url = $email_and_filename_decoded['file'];
            $filepath = dirname(__DIR__) . '/' . $url;
            error_log($filepath);
            error_log(file_get_contents('php://input'));
            header("text/html; charset=UTF-8");
            $this->sendIfPost($filepath, $receiver);
        }

    }

    public function sanitizeEmail($field) {
        $field = filter_var($field, FILTER_SANITIZE_EMAIL);
        if(filter_var($field, FILTER_VALIDATE_EMAIL)) {
            error_log('email valid');
            return true;
        } else {
            error_log('email not valid');
            return false;
        }
    }

    public function sendIfPost($file, $reveicer) {
        $mail = new PHPMailer(true);

        if(!$this->sanitizeEmail($reveicer)) {
            error_log('invalid input');
        } else { //send email
            try {
                //Server settings
                $mail->SMTPDebug = SMTP::DEBUG_OFF;                      // Enable verbose debug output
                $mail->isSMTP();                                            // Send using SMTP
                $mail->Host = 'smtp.gmail.com';                    // Set the SMTP server to send through
                $mail->SMTPAuth = true;                                   // Enable SMTP authentication
                $mail->Username = 'myadress@gmail.com';                     // SMTP username
                $mail->Password = 'mypassword';                               // SMTP password
                $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
                $mail->Port = 587;
                $mail->SMTPSecure = 'tls';// TCP port to connect to

                //Recipients
                $mail->setFrom('myadress@gmail.com', 'Mailer');
                $mail->addAddress($reveicer, 'Tester');     // Add a recipient
                //  $mail->addAddress('ellen@example.com');               // Name is optional
                $mail->addReplyTo('myadress@gmail.com', 'Information');
                //   $mail->addCC('cc@example.com');
                //  $mail->addBCC('bcc@example.com');

                // Attachments
                $mail->addAttachment($file);         // Add attachments
                //  $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

                // Content
                $mail->isHTML(true);                                  // Set email format to HTML
                $mail->Subject = 'Here is the subject';
                $mail->Body = 'This is the HTML message body <b>in bold!</b>';
                $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

                $mail->send();
                error_log('Message has been sent');
                $this->returnSuccess("gesendet");
                // echo "bla";
            } catch(Exception $e) {
                error_log('Message could not be sent. Mailer Error: ' . $mail->ErrorInfo);
                $this->returnError('mail konnte nicht gesendet werden');
            }
        }
    }

    public function returnSuccess($err_msg = '') {

        Url::addStatusHeader(200);
        header('Content-Type: application/json');

        echo json_encode([
            'success' => true,
            'msg' => $err_msg,
        ]);
    }

    public function returnError($err_msg = '') {

        Url::addStatusHeader(501);
        header('Content-Type: application/json');

        echo json_encode([
            'success' => false,
            'msg' => $err_msg,
        ]);
    }
}