<?php
namespace Invoice;

use Flood\Captn\EventDispatcher;
class MYPDF extends \TCPDF {
    public $footer_template;
    public $footer_data_array;
    public $header_template;
    
    public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false, $pdfa = false, $footer_template, $footer_data_array, $header_template ) {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $pdfa);
        $this->footer_template = $footer_template;
        $this->footer_data_array = $footer_data_array;
        $this->header_template = $header_template;
    }

    // Page footer
    public function Footer() {
        $this->SetY(-20);
        $this->SetFont('helvetica', 'N', 8);
        $result = EventDispatcher::trigger('core.render.file', ['tpl' => $this->footer_template, 'data' => $this->footer_data_array]);
        $html_footer = $result['rendered'];
        $this->writeHTML($html_footer, true, 0, true, 0);
    }

    public function Header() {
        ///$this->SetX(-20);
        $this->SetFont('helvetica', 'N', 8);
        $result = EventDispatcher::trigger('core.render.file', ['tpl' => $this->header_template, 'data' => []]);
        $html_header = $result['rendered'];
        $this->writeHTML($html_header, true, 0, true, 0);
    }

}