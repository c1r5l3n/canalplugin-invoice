1. mail template files into folders
2. naming control
3. cleaning

## Use Cases

1. Vorschaugenerierung fuer interne Zwecke und Entwicklung
2. Geniere Datei und Speichere PDF dort
3. Gebe gespeicherte PDF aus - wenn berechtigt
4. Zeige Rechnung als HTML an
5. Verschicke Rechnung per E-Mailanhang (pdf)
5. Verschicke Rechnung per E-Mailinhalt (html)

> keine sorge um zugangsschutz, nur controller bauen
