<?php


namespace Invoice;


use Flood\Captn\EventDispatcher;

class InvoiceGenerator {

    public $font_family;
    public $font_style;
    public $font_size;
    public $author;
    public $title;
    public $subject;
    public $pdf;
    public $store_dir;
    public $html;
    public $footer_template;
    public $footer_data_array;
    public $header_template;

    public function __construct($font_family, $font_size, $font_style, $author, $title, $subject, $store_dir, $footer_template, $footer_data_array, $header_template) {
        $this->font_family= $font_family;
        $this->font_size = $font_size;
        $this->font_style = $font_style;
        $this->author = $author;
        $this->title = $title;
        $this->subject = $subject;
        $this->store_dir = $store_dir;
        $this->footer_template = $footer_template;
        $this->footer_data_array = $footer_data_array;
        $this->header_template = $header_template;
        $this->pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false, false, $this->footer_template, $this->footer_data_array, $this->header_template);
    }

    public function renderContent($page_template, $page_data) {
        $result = EventDispatcher::trigger('core.render.file',
            ['tpl' => $page_template, 'data' => $page_data]);
        return $result['rendered'];
    }
    
    
    public function formatPdf($language) {
        $this->pdf->AddPage();
        $this->pdf->SetCreator(PDF_CREATOR);
        $this->pdf->SetAuthor($this->author);
        $this->pdf->SetTitle($this->title);
        $this->pdf->SetSubject($this->subject);
        $this->pdf->setFooterFont([PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA]);
        $this->pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $this->pdf->SetY(0, true, true);
        //$this->pdf->setPrintHeader(false);
       $this->pdf->setHeaderMargin(PDF_MARGIN_HEADER);
       $this->pdf->setTopMargin(20);
        $this->pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $this->pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);
        $this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        if(@file_exists(dirname(__FILE__) . '/lang/'.$language.'.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $this->pdf->setLanguageArray($language);
        }
        $this->pdf->SetFont($this->font_family, '', $this->font_size);
        $this->pdf->lastPage();
    }

    public function outputPdf($page_template, $page_data, $filename) {
        $pdf_dir = $_SERVER['DOCUMENT_ROOT'] . $this->store_dir;
        try {
            $this->pdf->writeHTML($this->renderContent($page_template, $page_data), true, 0, true, 0);
        } catch(Throwable $e) {
            error_log($e->getMessage());
            error_log($e->getLine());
            error_log($e->getFile());
        }
        $this->pdf->Output($pdf_dir . '/' . $filename . '.pdf', 'FI');
    }
}