<?php


namespace invoice;


use Flood\Canal\AppService\PageController;
class ViewFile extends PageController {
    private $preview_dir;
    private $basepath_string = '/data/out_private/';
    private $subdirectory = "pdf1/pdf2/";

    /**
     * ViewPDF constructor.
     *
     * @param \Flood\Canal\Frontend $frontend
     */
    public function __construct(\Flood\Canal\Frontend $frontend) {
        parent::__construct($frontend);
        $this->path_template = 'ViewFile.twig';
        $this->preview_dir = dirname(__DIR__, 4) . $this->basepath_string;

        /*
         * A demo AppService.method
         */
        $this->view();
        $this->assign('pdfArray', $this->getAllDownloadFiles());
        $this->assign('rootpath', '../..' . $this->basepath_string);
        $this->assign('subdirectory', $this->subdirectory);
    }

    public function getAllDownloadFiles() {
        $all_pdfs = scandir($this->preview_dir);
        $filtered_array = [];
        foreach($all_pdfs as $file) {
            if(pathinfo($file)['extension'] === "pdf") {
                $filtered_array[] = $file;
            }
        }
        return $filtered_array;
    }
}



