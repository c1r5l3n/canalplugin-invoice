<?php


namespace Invoice;


class PDF {
    public $path;

    public function __construct() {
        $this->path = dirname(__DIR__, 4) . substr($_SERVER['REQUEST_URI'], 4);
    }

    public function downloadPDF() {
        header("Content-Description: File Transfer");
        header("Content-Type: application/pdf");
        header('Content-Disposition: attachment; filename=' . basename($this->path));
        error_log($this->path);
        readfile($this->path);
    }

    public function respond() {
        if(0) {
            // throw new UserException\AccessException('');
        } else {
            return $this->downloadPDF();
        }
    }
}
