<?php

use Flood\Captn\EventDispatcher;
use Invoice\InvoiceGenerator;

return static function($data) {
    if($_POST['submit_invoice']) {
        $order = json_decode(file_get_contents(__DIR__ . '/order.json'), true);
        $invoice_generator = new InvoiceGenerator('helvetica', '9',
            'N', 'bemit UG (haftungsbeschränkt)', 'Your Shop - Invoice', 'invoice', 'data/out_private' , 'footer_3_columns.twig', [], 'header_backgroundcolor.twig');
        $invoice_generator->formatPdf('eng');
        $filename = 'your_invoice';
        $invoice_generator->outputPdf('/mail/content_invoice.twig', ["order" => $order, "prefix" => "order"], $filename);
    }


};

