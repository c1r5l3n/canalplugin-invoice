# Invoice-generator plugin for flood/canal

## Usage

This block ist for a Flood\Canal project. It:

- generates invoices in PDF
- includes a controller for showing a preview (`Viewfile.php`) and a download button for
- sending the invoice per mail via SMTP (Controller: `Mail.php`). 

There is a dummy order.json which contains the data for the first dummy invoice.

## How to integrate it to your canal-project

You have to add it to `_content.php`

Add the following code to the function:

        $content->addBlock('invoice', __DIR__ . '/vendor/canalplugin/invoice/src');

You have to add the folder `views` to Twig-folders in  `_config.php`. 
 
Add the following code: 
    
        ->addViewDir(__DIR__ . '/vendor/canalplugin/invoice/src/views') 
        
Move the files from the folder "to-be-moved" into the designed directory. 

Add the following routes to `_route.php`:

               'view_file' => [
                   'path' => '/view_file',
                   'controller' => function($frontend) {
                       $controller = new Invoice\ViewFile($frontend);
                       $controller->view();
                       return $controller->respond('ViewFile.twig');
                   },
               ],
               'pdf' => [
                   'path' => '/pdf/data/out_private/{page}',
                   'controller' => function() {
                       $controller = new Invoice\PDF();
                       return $controller->respond();
                   },
                   'defaults' => ['rel' => 'nofollow, noindex'],
                   'requirements' => ['page' => ".+"],
               ],
               'invoice' => [
                   'path' => '/invoice',
                   'controller' => function() {
                       $controller = new Invoice\Invoice();
                       return $controller->echoContent();
       
                   },
               ],
               'mail' => [
                   'path' => '/mail',
                   'controller' => function() {
                       $controller = new Mail();
                       return $controller->sendMail();
       
                   },
               ],

In the Class `Mail.php`, we have taken Gmail as a sample SMTP-Server. Beside having to insert your sender email-adress and password, you will have to change host server if you don´t want to send the mail from a gmail-adress. To allow sending emails via php from gmail, you have to confirm "allow less secure apps" in security configuration of your Google account.

In this installation folder, we assume that the pdfs are saved to  `data/out_private`. If you want to change that, or if you want to change the name of the PDF, change it in `blockController.php`.

For sending the Email, you will have to enter your email-adress and password instead of dummy data in `Mail.php`.
         
## Licence

This project is free software distributed under the terms of two licences, the CeCILL-C and the GNU Lesser General Public License. You can use, modify and/ or redistribute the software under the terms of CeCILL-C (v1) for Europe or GNU LGPL (v3) for the rest of the world.

This file and the LICENCE.* files need to be distributed and not changed when distributing.
For more informations on the Licences which are applied read: [LICENCE.md](LICENCE.md)

## Copyright

Carolin Holat

## Thanks to TCPDF, which is used as a basis



